package examDevOps;

import static org.junit.Assert.*;



import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import org.junit.Test;

import examDevOps.DateUtils;


public class DateUtilsTest {
	

	@Test
	public void testAjoutJours_Positif()
	{
		Date resultat = DateUtils.ajoutJours("11/04/2023",3 );
		DateTime expected =DateTime.parse("14/04/2023", DateTimeFormat.forPattern("dd/MM/yyyy"));
		
		assertEquals(expected.toDate(),resultat);

	}	
	@Test 
	public void testAjoutJours_Zero()
	{
		Date resultat = DateUtils.ajoutJours("11/04/2023", 0);
		DateTime expected = DateTime.parse("11/04/2023",DateTimeFormat.forPattern("dd/MM/yyyy"));
		assertEquals(expected.toDate(),resultat);

	}
	  @Test(expected = IllegalArgumentException.class)
	    public void testAjoutJours_Negatif() {
	        DateUtils.ajoutJours("11/04/2023", -7);
	    }
	
}
